package vo;

public class PruebaVO implements Comparable<PruebaVO>{
	private String nombre; 
	private Integer agno; 
	private String[] generos;
	
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	public Integer getAgno(){
		return agno; 
	}
	public String getNombre(){
		return nombre; 
	}
	public void setAgno(Integer pAgno){
		agno = pAgno; 
	}
	public void setNombre(String pNombre){
		nombre = pNombre; 
	}
	@Override
	public int compareTo(PruebaVO that) {
		int respuesta = 0; 
		if (this.getAgno() <  that.getAgno() ){respuesta = -1;}
		if (this.getAgno() > that.getAgno()){respuesta =  1;}
		if (this.getAgno() == that.getAgno()) {respuesta = 0;} 
		
		if (this.getNombre().compareTo(that.getNombre()) < 0){ respuesta = -1;}
		if (this.getNombre().compareTo(that.getNombre()) > 0){ respuesta = 1;} 
		if (this.getNombre().compareTo(that.getNombre()) == 0){ respuesta = 0;}
		
		return respuesta; 
	}
	
	public boolean equals(PruebaVO that){
		if (this.getAgno() == that.getAgno() && this.getNombre().compareTo(that.getNombre()) == 0) return true; 
		else return false; 
	}
	
	

}
