package View;

import java.util.Scanner;

import Controller.Controller;
import data_structures.ColaPrioridad;
import model.MAXPQ;
import vo.PruebaVO;

public class View {

	private static long timestampinicio =0;
	private static long timestampfin =0;
	private static final String CARGARPELICULAS="./data/movies.csv";


	private static void printMenu()
	{
		System.out.println("1.Para generar datos en MAXPQ");
		System.out.println("2. Para generar datos en ColaPrioridad");
		System.out.println("3. Para saber cantidad de datos en el MAXPQ)");
		System.out.println("4. Para saber cantidad de datos en el ColaPrioridad"); 


	}

	public static void main(String[] args) {

		boolean fin=false;
		while (fin!=true) {
			printMenu();
			Scanner sc = new Scanner(System.in);
			int option = sc.nextInt();
			switch (option) {
			
			case 1 : 
				 timestampinicio = System.currentTimeMillis();
				System.out.println("Agregue la cantidad de datos de generar (bien)");
				Scanner cantidad = new Scanner(System.in); 
				int n2 = cantidad.nextInt(); 
				String[] data = Controller.generarCadenasBien(n2);
				for (int i = 0; i < data.length; i++){
					System.out.println("" + data[i].toString());
				}
				String [] dataEnviar = data;
				Controller.enviarData(dataEnviar);
				 timestampfin = System.currentTimeMillis();
				System.out.println("El tiempo del metodo fue de: " +((timestampfin-timestampinicio)/1000)+" Segundo(s)");
				
				break;
			case 2 : 
				timestampinicio = System.currentTimeMillis();
				System.out.println("Agregue la cantidad de datos de generar (bien)");
				Scanner cantidad2 = new Scanner(System.in); 
				int n3 = cantidad2.nextInt(); 
				String[] data2 = Controller.generarCadenasBien(n3);
				for (int i = 0; i < data2.length; i++){
					System.out.println("" + data2[i].toString());
				}
				String [] dataEnviar2 = data2;
				Controller.enviarDataColaDePrioridad(dataEnviar2);
				 timestampfin = System.currentTimeMillis();
				System.out.println("El tiempo del metodo fue de: " +((timestampfin-timestampinicio)/1000)+" Segundo(s)");
				
			case 3:
				 timestampinicio = System.currentTimeMillis();
				int cc = Controller.cantidadDeDatos();
				MAXPQ<PruebaVO> pruebas = Controller.darElementos();
				System.out.println(""+cc);
				System.out.println(""+pruebas.max().getNombre());
				 timestampfin = System.currentTimeMillis();
				System.out.println("El tiempo del metodo fue de: " +((timestampfin-timestampinicio)/1000)+" Segundo(s)");
				
				
				break;
			case 4:
				timestampinicio = System.currentTimeMillis();
				int cc1 = Controller.cantidadDeDatos();
				ColaPrioridad<PruebaVO> pruebas1 = Controller.darElementosColaPrioridad();
				System.out.println(""+cc1);
				System.out.println(""+pruebas1.delMax().getNombre());
				 timestampfin = System.currentTimeMillis();
				System.out.println("El tiempo del metodo fue de: " +((timestampfin-timestampinicio)/1000)+" Segundo(s)");
				
				break;
			case 5:
				Controller.cagarArchivos(CARGARPELICULAS);
				break;
			default:
				fin=true;
				break;
			}
		}

	}

}
