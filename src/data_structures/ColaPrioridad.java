package data_structures;



public class ColaPrioridad<T extends Comparable<T>> {
    private T[] pq;      // elements
    private int n;         // number of elements

    // set inititial size of heap to hold size elements
    public void crearCP(int capacity) {
        pq = (T[]) new Comparable[capacity];
        n = 0;
    }

    public boolean isEmpty()   { return n == 0; }
    public int size()          { return n;      }
    public void insert(T x)  { pq[n++] = x;   }

    public T delMax() {
        int max = 0;
        for (int i = 1; i < n; i++)
            if (less(max, i)) max = i;
        exch(max, n-1);

        return pq[--n];
    }


   /***************************************************************************
    * Metodos auxiliares
    ***************************************************************************/
    private boolean less(int i, int j) {
        return pq[i].compareTo(pq[j]) < 0;
    }

    private void exch(int i, int j) {
        T swap = pq[i];
        pq[i] = pq[j];
        pq[j] = swap;
    }

}