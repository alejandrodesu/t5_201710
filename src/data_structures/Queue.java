package data_structures;


public class Queue<T> implements IQueue<T> {
ListaEncadenada lista;
	
	public Queue()
	{
		lista = new ListaEncadenada<T>();
	}
	@Override
	public void enqueue(T elem) 
	{
		lista.agregarElementoFinal(elem);
	}

	@Override
	public T dequeue() 
	{
		return (T) lista.quitarElementoPrincipio();
	}

	@Override
	public int size() 
	{
		return lista.darNumeroElementos();
	}

	@Override
	public boolean isEmpty() 
	{
		if(lista.darNumeroElementos()==0)
		{
			return true;
		}
		else
			return false;
	}
	
	public T darElemento(int pos)
	{
		return (T) lista.darElemento(pos);
	}

}
