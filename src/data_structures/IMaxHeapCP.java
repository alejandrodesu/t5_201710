package data_structures;

public interface IMaxHeapCP <T>{
	/*
	 * Crea una cola de prioridad vacía con un número
	 *máximo de elementos
	 */
	public void crearCP(int max);
	/*
	 * Retorna número de elementos en la cola de
	 *prioridad
	 */
	public int darNumElementos(); 
	/*
	 * Agrega un elemento a la cola. Se genera
	 *Exception en el caso que se sobrepase el
	 *tamaño máximo de la cola
	 */
	public void agregarElemento(T elemeto); 
	/*
	 * Saca/atiende el elemento máximo en la cola
	 *y lo retornar; null en caso de cola vacía
	 */
	public T max(); 
	/*
	 * dice si la cola esta vacia o no 
	 */
	public boolean esVacia(); 
	/*
	 * retorna el tamaño maximo de la cola 
	 */
	public int tamanoMax(); 
}
