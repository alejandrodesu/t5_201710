package data_structures;

import java.util.Iterator;


public class ListaEncadenada<T> implements ILista<T> {
	private Node<T> primero;
	private Node<T> actual;
	private Node<T> next; 
	private int listSize;
	
	public ListaEncadenada()
	{
		primero = new Node<T>(null);
		actual = primero;
		listSize=0;
	}
	
	public void cambiarActualAPrimero()
	{
		actual=primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
			{
			private Node<T> act=null;
	
			@Override
			public boolean hasNext() 
			{
				if(act==null)return primero.getItem()!=null;
				else return act.getNext() != null;
			}
	
			@Override
			public T next() 
			{
				if(act==null)
				{
					act=primero;
					if(act==null)return null;
					else return act.getItem();
				}
				else
				{
					act = act.getNext();
					return act.getItem();
				}
	
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
}
	public Node<T> darPrimero(){return primero;}
	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			Node<T> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setNext(new Node<T>(elem));
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}
	
	public void agregarElementoPrincipio(T elem)
	{
		Node<T> newNode = new Node(elem);
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			newNode.setNext(primero);
			primero=newNode;
		}
		listSize++;
	}
	
	public T quitarElementoPrincipio()
	{
		if(primero == null)
		{
			System.out.println("No hay elementos");
		}
		T elem = primero.getItem();
		Node<T> siguientePrimero = primero.getNext();
		primero.setItem(null);
		primero = siguientePrimero;
		listSize--;
		return elem;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		Node<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	
	public boolean retrocederPosicionAnterior() 
	{
		Node<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}
	
	public boolean isEmpty() {
		if (primero==null) {
			return true;
		}
		return false;
	}

	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	public void modificar(T darElemento, int i) {
		// TODO Auto-generated method stub
		Node<T> tempActual = primero;
		int contador = 0;
		while(contador<i)
		{
			tempActual = tempActual.getNext();
			contador++;
		}
		tempActual.setItem(darElemento);
	}
}
