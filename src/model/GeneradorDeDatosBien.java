package model;

import java.util.Random;

import vo.PruebaVO;

public class GeneradorDeDatosBien {

	private MAXPQ<PruebaVO> maxpq = new MAXPQ<>();
	
	
	PruebaVO[] cadena; 
	Random rnd = new Random(); 
	public String getCadenaAlfanumAleatoria (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( c >='A' && c <='Z' ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
	}

	public String[] generarCadena(int n){ 
		String[] vector = new String[n]; 
		for (int i = 0 ; i<n; i++){
			PruebaVO actual = new PruebaVO();
			int agno = (int) (rnd.nextDouble() * 67 + 1950); 
			int longitudCadena = (int) (rnd.nextDouble() * 15 + 1); 
			String cadena = getCadenaAlfanumAleatoria(longitudCadena); 
			vector[i] = cadena +" "+ agno; 
		}
		return vector; 
	}
	
	
}
