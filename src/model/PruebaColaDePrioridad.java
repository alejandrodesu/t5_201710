package model;



import data_structures.ColaPrioridad;
import vo.PruebaVO;

public class PruebaColaDePrioridad 
{
	private GeneradorDeDatos generador = new GeneradorDeDatos(); 
	private ColaPrioridad<PruebaVO> cola = new ColaPrioridad<>(); 
	private MAXPQ<PruebaVO> maxpq = new MAXPQ<>();
	private PruebaVO[] array; 
	private String [] arrayDeDatos;

	public void generarMuestra(int n){
		arrayDeDatos=generador.generarCadena(n);
		array = new PruebaVO[n];
	}
	
	public void crearMAXPQ(String [] data) {
		maxpq.crearCP(data.length);
		for (int i = 0; i < data.length; i++) {
			PruebaVO prueba = new PruebaVO();
			String datos[] = data[i].split(" ");
			String titulo = datos[0];
			int agnoAux = Integer.parseInt(datos[1]);
			prueba.setNombre(titulo);
			prueba.setAgno(agnoAux);
			maxpq.agregarElemento(prueba);
		}
	}
	
	public int cantidadDeDatos() {
		return maxpq.darNumElementos();
	}
	
	public MAXPQ<PruebaVO> darElementos()
	{
		return maxpq;
	}

	public void crearCola(String []data){
		cola.crearCP(data.length);
		for (int i = 0; i < data.length; i++) {
			PruebaVO prueba = new PruebaVO();
			String datos[] = data[i].split(" ");
			String titulo = datos[0];
			int agnoAux = Integer.parseInt(datos[1]);
			prueba.setNombre(titulo);
			prueba.setAgno(agnoAux);
			cola.insert(prueba);
		}
	}
	public int tamanioColaPrioridad()
	{
		return cola.size();
	}
	public ColaPrioridad<PruebaVO> darlementosColaprioridad()
	{
		return cola;
	}

}
