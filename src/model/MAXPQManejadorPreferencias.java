package model;

import data_structures.IMaxHeapCP;

public class MAXPQManejadorPreferencias <T extends Comparable<T>> implements IMaxHeapCP<T> {
	private T[]pq;
	private int size;
	
	
	@Override
	public void crearCP(int max) {
		pq = (T[]) new Comparable[max+1];
	}

	@Override
	public int darNumElementos() {
		return size;
	}

	@Override
	public void agregarElemento(T elemeto) {
		pq[++size]=elemeto;
		swim(size);

	}

	private void swim(int k) {
		while (k>1&&less(k/2,k)){
			exch(k/2, k);
			k=k/2;
		}
	}

	@Override
	public T max() {
		if(esVacia()==true)return null;
		T Max = pq[1];
		exch(1, size--);
		pq[size+1]=null;
		sink(1);
		return Max;
	}

	@Override
	public boolean esVacia() {
		return size==0;
	}

	@Override
	public int tamanoMax() {
		if(esVacia()==true)return 0;
		return pq.length;
	}


	private boolean  less(int i, int j) 
	{  
		return pq[i].compareTo(pq[j]) < 0;  
	} 


	private void  exch(int i, int j) 
	{  
		T t = pq[i]; pq[i] = pq[j]; pq[j] = t;  
	}


	private void sink(int k) 
	{   
		while (2*k <= size)
		{   
			int j = 2*k; 
			if (j < size && less(j, j+1)) j++;
			if (!less(k, j)) break;  
			exch(k, j);
			k = j;
		} 
	}



}
