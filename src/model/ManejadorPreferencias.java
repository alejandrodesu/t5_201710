package model;

import java.io.BufferedReader;
import java.io.FileReader;

import data_structures.ColaPrioridad;
import data_structures.ListaEncadenada;
import vo.PruebaVO;

public class ManejadorPreferencias {


	ColaPrioridad<PruebaVO> pruebas = new ColaPrioridad<>();
	ListaEncadenada<PruebaVO> peliculas = new ListaEncadenada<>();

	public boolean cargarArchivo(String ruta)
	{
		BufferedReader buffer = null;
		FileReader file = null;
		try
		{
			file = new FileReader(ruta);
			buffer = new BufferedReader(file);
			String line = buffer.readLine();
			line = buffer.readLine();
			while(line != null)
			{

				String[] datos = new String[4];
				String nombre;
				String generos;
				if(line.indexOf('"') != -1)
				{
					nombre = line.substring(line.indexOf('"') +1 , line.lastIndexOf('"')).trim();
					line = line.substring(0, line.indexOf('"')) + line.substring(line.lastIndexOf('"') + 1, line.length());
					datos = line.split(",");
					generos = datos[1];
				}
				else
				{
					datos = line.split(",");
					nombre = datos[1].trim();
					generos = datos[2];
				}

				String[] arrayGeneros = generos.split("|");
				String titulo = nombre.substring(0, nombre.length()-7);
				String agno = "0";
				if(nombre.charAt(nombre.length()-1) == ')' )
				{
					if(nombre.charAt(nombre.length()-6) == '(')
					{
						agno = nombre.substring(nombre.length()-5, nombre.length()-1);
					}
					else if(nombre.charAt(nombre.length()-6) == '-')
					{
						agno = nombre.substring(nombre.length()-10, nombre.length()-6);
					}
					else if(nombre.charAt(nombre.length()-2)=='-')
					{
						agno = nombre.substring(nombre.length()-6, nombre.length()-2);
					}
				}
				PruebaVO act = new PruebaVO();
				act.setNombre(titulo);
				int agnio = Integer.parseInt(agno);
				act.setAgno(agnio);
				act.setGeneros(arrayGeneros);
				peliculas.agregarElementoPrincipio(act);
				line = buffer.readLine();

			} 
		}catch (Exception e) {
			// TODO: handle exception
		}
		return true;

	}

	public PruebaVO[] LinkedListToArray()
	{
		PruebaVO[] prueba = new PruebaVO[peliculas.darNumeroElementos()];
		for (int i = 0; i < peliculas.darNumeroElementos(); i++) {
			peliculas.cambiarActualAPrimero();
			prueba[i]=peliculas.darElementoPosicionActual();
			peliculas.avanzarSiguientePosicion();
		}
		return prueba;
	}
	
	public PruebaVO[] porGenero(String Genero)
	{
		PruebaVO[] prueba = LinkedListToArray();
		PruebaVO[] pruebaPorGeneros = new PruebaVO[prueba.length];
		String [] generos = new String[10];
		for (int i = 0; i < prueba.length; i++) {
			PruebaVO pruebaVO = new PruebaVO();
			for (int j = 0; j < pruebaVO.getGeneros().length; j++) {
				generos = pruebaVO.getGeneros();
				if(generos[j].equalsIgnoreCase(Genero))
				{
					break;
				}
				pruebaPorGeneros[i]=pruebaVO;
			}
		}
		return pruebaPorGeneros;
	}
	
	public ColaPrioridad<PruebaVO> agregarCola(String Genero)
	{
		PruebaVO[] pruebaPorGeneros = porGenero(Genero);
		pruebas.crearCP(pruebaPorGeneros.length);
		for (int i = 0; i < pruebaPorGeneros.length; i++) {
			pruebas.insert(pruebaPorGeneros[i]);
		}
		return pruebas;

	}

}
